<?php
include "webappointmentmanager.inc.php";

//*****************************************************************************
class page extends webappointmentmanager
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
		
		
	//**************************************************************************
	function __construct()
		{
		parent::__construct();
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->updateRecord();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Preferenze", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		
		$this->form = $this->getForm();
		
		$ctrl = $this->form->addBoolean("navigation_by_windows", "Navigazione a finestre", false);
			$ctrl->dbBound = false;
			$ctrl->value = $this->userPreferences["navigation_by_windows"];
			
		$ctrl = $this->form->addSelect("max_table_rows", "Righe per pagina", false, true);
			$ctrl->dbBound = false;
			$ctrl->emptyRow = false;
			for ($i = 10; $i <= 100; $i += 10)
				$ctrl->list[$i] = $i;
			$ctrl->value = $this->userPreferences["max_table_rows"];

//		$ctrl = $this->form->addSelect("table_actions", "Azioni tabella", false, true);
//			$ctrl->dbBound = false;
//			$ctrl->emptyRow = false;
//			$ctrl->list = array("watable_actions_left" => "a sinistra", "watable_actions_context" => "menu contestuale tasto destro", "watable_actions_context_left_click" => "menu contestuale tasto sinistro", "watable_actions_right" => "a destra");
//			$ctrl->value = $this->userPreferences["table_actions"];
		
		$this->form_submitButtons($this->form, false, false);
		
		$this->form->getInputValues();

		}

	//***************************************************************************
	function updateRecord()
		{
		$this->checkMandatory($this->form);
		
		// blob della configurazione da mandare via cookie al browser
		$prefs["navigation_by_windows"] = $this->form->navigation_by_windows;
		$prefs["max_table_rows"] = $this->form->max_table_rows;
//		$prefs["table_actions"] = $this->form->table_actions;
		$cookieVal = base64_encode(serialize($prefs));
		setcookie($this->name . "_prefs", $cookieVal, mktime(0,0,0, date('n'), date('j'), date('Y') + 10), "$this->httpwd", $this->domain);
			
		$this->response();
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
