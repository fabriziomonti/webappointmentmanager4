<?php
//*****************************************************************************
include "webappointmentmanager.inc.php";

//*****************************************************************************
class page extends webappointmentmanager
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
	
	var $config;
	var $rsOrari;
	
	//**************************************************************************
	function __construct()
		{
		parent::__construct();
		
		$this->setConfig();
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->updateRecord();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Scheda creazione periodo appuntamenti", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		$this->form = $this->getForm();
		$readOnly = false;

		$this->form->addDate("DallaData", "Dalla data", $readOnly, true)
			->value = $this->getDefaultStart();
		$this->form->addDate("AllaData", "Alla data", $readOnly, true)->
			value = $this->getDefaultEnd($this->form->inputControls["DallaData"]->value);
		
		$this->form_submitButtons($this->form, false, false);

		$this->form->getInputValues();
		}

	//*****************************************************************************
	function getDefaultStart()
		{
		$dbconn = $this->getDBConnection();
		// cerchiamo l'ultimo appuntamento valido e gli sommiamo un giorno
		$sql = "SELECT MAX(DataAppuntamento) AS LastApp FROM Appuntamenti" .
				" WHERE IDUtente=" . $dbconn->sqlInteger($this->user->IDUtente) .
				" AND IDStatoAppuntamento<>" . $dbconn->sqlInteger(APPL_APP_ATTESA_VALIDAZIONE) .
				" AND Sospeso<>1";
		$record = $this->getRecordset($sql, $dbconn, 1)->records[0];
	
		if ($record && $record->LastApp)
			{
			return $record->LastApp + (60 * 60 * 24);
			}
		return time();
		}
		
	//*****************************************************************************
	function getDefaultEnd($start)
		{
		// la data di fine di default e' un mese dalla data di partenza,
		// oppure un massimo di tre mesi dalla data corrente
		$end = mktime(0,0,0, date('n', $start) + 1, date('j', $start) - 1, date('Y', $start));
		$max = mktime(0,0,0, date('n') + 3, date('j'), date('Y'));
		return $end > $max ? $max : $end;
		}
			
	//***************************************************************************
	function updateRecord()
		{
		// controlli obbligatorieta' e formali
		$this->checkMandatory($this->form);
		
		$dbconn = $this->config->recordset->dbConnection;
		$dbconn->beginTransaction();
		
		// cancelliamo tutti gli appuntamenti eventualmente non confermati
		// in precedenza
		$sql = "DELETE FROM Appuntamenti" .
				" WHERE IDUtente=" . $dbconn->sqlInteger($this->user->IDUtente) .
				" AND IDStatoAppuntamento=" . $dbconn->sqlInteger(APPL_APP_ATTESA_VALIDAZIONE);
		$this->dbExecute($sql, $dbconn);
		
		// per ognuna delle giornate individuate dal periodo, creiamo un record di
		// appuntamento
		$dateCur = $this->form->DallaData;
		$dateEnd = $this->form->AllaData;
		while ($dateCur <= $dateEnd)
			{
			foreach ($this->rsOrari->records as $orario)
				{
				if (date('w', $dateCur) == $orario->IDGiorno)
					{
					$oraCur = $orario->DalleOre;
					$oraEnd = $orario->AlleOre;
					while ($oraCur < $oraEnd)
						{
						$sql = "INSERT INTO Appuntamenti (IDUtente, DataAppuntamento, DalleOre, AlleOre, IDStatoAppuntamento, DataOraUltimaModifica, IPUltimaModifica)" .
								" VALUES (" .
								$dbconn->sqlInteger($this->user->IDUtente) . ", " .
								$dbconn->sqlDate($dateCur) . ", " .
								$dbconn->sqlTime($oraCur) . ", " .
								$dbconn->sqlTime($oraCur + ($this->config->DurataMediaMinuti * 60)) . ", " .
								$dbconn->sqlInteger(APPL_APP_ATTESA_VALIDAZIONE) . ", " .
								$dbconn->sqlDate(time()) . ", " .
								$dbconn->sqlString($_SERVER['REMOTE_ADDR']) . 
								");\n";
						$this->dbExecute($sql, $dbconn);
						$oraCur += ($this->config->DurataMediaMinuti * 60) + ($this->config->MinutiPausa * 60);
						}
					}
				}
			$dateCur += (60 * 60 * 24);
			}
						
		$dbconn->commitTransaction();
		$this->response();
		}
		
	//***************************************************************************
	function setConfig()
		{
		$dbconn = $this->getDBConnection();
		
		// leggiamo la configurazione
		$sql = "SELECT * FROM Config" .
				" WHERE IDUtente=" . $dbconn->sqlInteger($this->user->IDUtente) .
				" AND Sospeso<>1";
		$this->config = $this->getRecordset($sql, $dbconn, 1)->records[0];
		if (!$this->config)
			{
			$this->showMessage("Configurazione non definita", "La configurazione prenotazione non e' ancora stata definita. " .
					"Sei pregato di provvedere tramite <a href='formconfig.php'>Configurazione prenotazione</a>.", false, true);
			}

		// leggiamo gli orari prenotazione
		$sql = "SELECT * FROM OrariRicevimento" .
				" WHERE IDUtente=" . $dbconn->sqlInteger($this->user->IDUtente) .
				" AND Sospeso<>1";
		$this->rsOrari = $this->getRecordset($sql, $dbconn);
		if (!$this->rsOrari->records)
			{
			$this->showMessage("Orari non definiti", "La configurazione degli orari di ricevimento utente non e' ancora stata definita. " .
					"Sei pregato di provvedere tramite la voce di menu <b>Configurazione -> Orari ricevimento</b>.", false, true);
			}

		}
		
	//*****************************************************************************
	//*****   funzioni rpc   ******************************************************
	//*****************************************************************************
	function rpc_checkExists($DallaData, $AllaData)
		{
		$dbconn = $this->getDBConnection();
		$sql = "SELECT * FROM Appuntamenti" .
				" WHERE IDUtente=" . $dbconn->sqlInteger($this->user->IDUtente) .
				" AND DataAppuntamento>=" . $dbconn->sqlDate($DallaData) .
				" AND DataAppuntamento<=" . $dbconn->sqlDate($AllaData) .
				" AND IDStatoAppuntamento<>" . $dbconn->sqlInteger(APPL_APP_ATTESA_VALIDAZIONE) .
				" AND Sospeso<>1";

		$record = $this->getRecordset($sql, $dbconn, 1)->records[0];
		return $record ? 1 : 0;
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
