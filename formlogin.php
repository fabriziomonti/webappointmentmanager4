<?php
include "webappointmentmanager.inc.php";

//*****************************************************************************
class page extends webappointmentmanager
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
	
	//**************************************************************************
	function __construct()
		{
		parent::__construct(false);
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->doLogin();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* showPage
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("$this->title Login", "title");
		$this->addItem("(se hai dimenticato la password lascia il campo vuoto: sarà inviata al tuo indirizzo email)", "subtitle");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		$this->form = $this->getForm();
		$this->form->addText("Login", "Login Utente", false, true);
		$this->form->addPassword("Password", "Password");
		
		new waLibs\waButton($this->form, 'cmd_submit', ' Login ');
		
		$this->form->getInputValues();
		}
	
	//***************************************************************************
	function doLogin()
		{
		$this->checkMandatory($this->form);
		
		$dbconn = $this->getDBConnection();
		$sql = "select Utenti.*" .
				" from Utenti" .
				" where Utenti.Login=" . $dbconn->sqlString($this->form->Login) .
				" and not Utenti.Sospeso";
		$record = $this->getRecordset($sql, $dbconn, 1)->records[0];
		if (!$record || $this->form->Password && $record->Password != $this->form->Password)
			{
			$this->showMessage("Utente non riconosciuto", "Login non permesso: utente non riconosciuto");
			}

		if (!$this->form->Password)
			{
			$this->sendMailPassword($record);
			$this->showMessage("Credenziali inviate", "Le credenziali sono state inviate all'indirizzo registrato per la login utente $record->Login");
			}

		$this->user = $this->record2Object($record);
		
		$this->redirect($this->startPage);
		}
			    
//*****************************************************************************
	}
	
//*****************************************************************************
// istanzia la pagina
$page = new page();
