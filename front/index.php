<?php
//******************************************************************************
include "front.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends front
	{
		
	//*****************************************************************************
	function __construct()
		{
		parent::__construct();

		$this->addItem("Appuntamenti disponibili", "title");
		$this->addItem($this->getTable());
		$this->show();
		}

	//*****************************************************************************
	/**
	 * @return waLibs\waTable
	 */
	function getTable()
		{
		// creazione della tabella
		$dbconn = $this->getDBConnection();

		$sql = "SELECT Appuntamenti.*," .
				" Giorni.DescrizioneGiorno" .
				" FROM Appuntamenti" .
				" INNER JOIN Giorni ON DATE_FORMAT(Appuntamenti.DataAppuntamento, '%w')=Giorni.IDGiorno".
				" LEFT JOIN Config ON Appuntamenti.IDUtente=Config.IDUtente" .
				" WHERE NOT Appuntamenti.Sospeso" .
				" AND Appuntamenti.IDUtente=" . $dbconn->sqlInteger($this->user->IDUtente) .
				" AND Appuntamenti.IDStatoAppuntamento=" . $dbconn->sqlInteger(APPL_APP_LIBERO) .
				" AND Appuntamenti.DataAppuntamento>=ADDDATE(CURRENT_DATE(), IFNULL(Config.NrGiorniPrenotazione, 0))" .
				" ORDER BY Appuntamenti.DataAppuntamento, Appuntamenti.DalleOre";
		
		$table = new waLibs\waTable($sql, $this->fileConfigDB);
		include_once __DIR__ . "/../ui/view/front/watable/watable.php";
		$table->view = new \webappointmentmanager\front\waTable_view();

		$table->removeAction("New");
		$table->removeAction("Details");
		$table->removeAction("Edit");
		$table->removeAction("Delete");
		$table->addAction("Prenota", true);
		
		$table->addColumn("IDAppuntamento", "ID", false, false, false)->aliasOf = "Appuntamenti.IDAppuntamento";
		
		$col = $table->addColumn("DescrizioneGiorno", "Giornata");
			$col->aliasOf = "Giorni.DescrizioneGiorno";
			$col->alignment = waLibs\waTable::ALIGN_C;
			
		$col = $table->addColumn("DataAppuntamento", "Data");
			$col->aliasOf = "Appuntamenti.DataAppuntamento";
		
		$col = $table->addColumn("DalleOre", "Dalle ore");
			$col->aliasOf = "Appuntamenti.DalleOre";
			$col->format = waLibs\waTable::FMT_TIME;
			
		$col = $table->addColumn("AlleOre", "Alle ore");
			$col->aliasOf = "Appuntamenti.AlleOre";
			$col->format = waLibs\waTable::FMT_TIME;

		// lettura dal database delle righe che andranno a popolare la tabella
		if (!$table->loadRows())
			{
			$this->showDBError($table->recordset->dbConnection);
			}

		return $table;
		}

		
	//*****************************************************************************
	}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new page();