<?php
include "front.inc.php";

//*****************************************************************************
class page extends front
	{
	/**
	 *
	 * @var waLibs\waRecord
	 */
	var $appuntamento;
		
		
	//**************************************************************************
	function __construct()
		{
		parent::__construct();
		
		// apriamo il file dato con la chiave data e leggiamo il contenuto
		$richiesta = $this->getEncryptString($_GET['k'], $_GET['f']);
		if (!$richiesta)
			{
			$this->showMessage("Errore riferimento prenotazione", "Si è verificato un errore durante la conferma della prenotazione." .
					" Si prega di avvertire l'assistenza tecnica o di riprovare più tardi.", false);
			}
		$richiesta = unserialize($richiesta);
		
		$this->appuntamento = $this->getAppuntamento($richiesta);
		$dbconn = $this->appuntamento->recordset->dbConnection;
		$dbconn->beginTransaction();
		$this->appuntamento->IDStatoappuntamento = APPL_APP_PRENOTATO;
		$this->setEditorData($this->appuntamento);
		$this->saveRecordset($this->appuntamento->recordset);
		$this->mandaMail($richiesta);
		$dbconn->commitTransaction();
		
		$msg = "La prenotazione dell'appuntamento in data <b>" . 
				$this->appuntamento->DescrizioneGiorno . " " .
				date('d/m/Y', $this->appuntamento->DataAppuntamento) .
				"</b> alle ore <b>" . date('H:i', $this->appuntamento->DalleOre) .
				"</b> è avvenuta correttamente.";
		if ($this->user->ConfermaAutomatica)
			{
			$msg .= "Salvo diverse comunicazioni, l'appuntamento è da ritenersi confermato.";
			}
		else
			{
			$msg .= "La preghiamo di attendere la conferma dell'appuntamento da parte di un incaricato.";
			}
			
		$msg .= " Grazie per avere utilizzato questo servizio.";
		$this->showMessage("Prenotazione effettuata", $msg, false);
		
		}
		
	//***************************************************************************
	/**
	* -
	*
	* @return waLibs\waRecord
	*/
	function getAppuntamento($richiesta)
		{
		$dbconn = $this->getDBConnection();
		$sql = "SELECT Appuntamenti.*," .
				" Giorni.DescrizioneGiorno" .
				" FROM Appuntamenti" .
				" INNER JOIN Giorni ON DATE_FORMAT(Appuntamenti.DataAppuntamento, '%w')=Giorni.IDGiorno".
				" WHERE Appuntamenti.IDAppuntamento=" . $dbconn->sqlInteger($richiesta["id"]) . 
				" AND Appuntamenti.IDUtente=" . $dbconn->sqlInteger($this->user->IDUtente) . 
				" AND Appuntamenti.Sospeso<>1"; 
		$record = $this->getRecordset($sql, $dbconn, 1)->records[0];
		if (!$record)
			{
			$this->showMessage("Nessun record", "Siamo spiacenti, l'appuntamento non è disponibile");
			}

		if ($record->IDStatoappuntamento != APPL_APP_LIBERO)
			{
			$this->showMessage("Non disponibile", "Siamo spiacenti, l'appuntamento non è più disponibile", true);
			}

		return $record;
		}
		
	//***************************************************************************
	function mandaMail($richiesta)
		{
		
		$body = "Ciao " . $this->user->Login . ",\r\n\r\n" .
				"questo e' un messaggio automatico generato dal servizio $this->title.\r\n\r\n" .
				"Ti comunico che hai ricevuto una richiesta di prenotazione appuntamento:\r\n\r\n" . 
				"data:     " . $this->appuntamento->DescrizioneGiorno . " " .
						date('d/m/Y', $this->appuntamento->DataAppuntamento) . "\r\n" .
				"ore:      " . date('H:i', $this->appuntamento->DalleOre) . "\r\n\r\n" .
	
				"nome:     $richiesta[Nome]\r\n" .
				"e-mail:   $richiesta[EMail]\r\n" .
				"telefono: $richiesta[Telefono]\r\n" .
				"via:      $richiesta[Via]\r\n" .
				"CAP:      $richiesta[CAP]\r\n" .
				"citta':   $richiesta[Citta]\r\n" .
				"prov.:    $richiesta[Provincia]\r\n" .
				"note:     $richiesta[Note]\r\n\r\n";
				
		if ($this->user->ConfermaAutomatica)
			{
			$body .= "L'appuntamento è da ritenersi confermato.";
			}
		else
			{
			$body .= "Il tuo utente attende conferma dell'appuntamento da parte tua.";
			}
			
		$body .= " Ciao da $this->title ($this->protocol://$this->domain$this->httpwd).";
	
		if (!$this->sendMail($this->user->Email, "Conferma prenotazione - $this->title", $body))
			{
			$this->showMessage("Errore invio messaggio email", "Si è verificato un errore durante l'invio del messaggio di posta elettronica" .
					" all'indirizzo " . $this->user->Email . 
					" Si prega di avvertire l'assistenza tecnica o di riprovare piu' tardi.", false);
			}
		}
	
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
