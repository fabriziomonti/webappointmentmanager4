<?php
//****************************************************************************************
include_once (__DIR__ . "/../webappointmentmanager.inc.php");


//****************************************************************************************
class front extends webappointmentmanager
	{

	//****************************************************************************************
	/**
	* costruttore
	 * 
	 */
	function __construct()
		{
		$this->domain = APPL_DOMAIN;
		$this->httpwd = APPL_DIRECTORY;
		$this->directoryTmp = APPL_TMP_DIRECTORY;
		$this->name = APPL_NAME;
		$this->title = APPL_TITLE;
		$this->version = APPL_REL;
		$this->versionDate = APPL_REL_DATE;
		$this->smtpServer = APPL_SMTP_SERVER;
		$this->smtpUser = APPL_SMTP_USER;
		$this->smtpPassword = APPL_SMTP_PWD;
		$this->smtpSecurity = APPL_SMTP_SECURE;
		$this->smtpPort = APPL_SMTP_PORT;
		$this->supportEmail = APPL_SUPPORT_ADDR;
		$this->infoEmail = APPL_INFO_ADDR;
		
		$this->directoryDoc = APPL_DOC_DIRECTORY;
		$this->webDirectoryDoc = APPL_WEB_DOC_DIRECTORY;
		
		$this->fileConfigDB = __DIR__ . "/../dbconfig.inc.php";
		
		self::$applicationInstance = $this;
		
		$this->sectionTitle = "front";
		$this->sectionName = "front";

		$this->init();
		
		// verifica validità dell'utente fornitore
		$this->checkUser();
		
		}
	
	//*****************************************************************************
	/**
	 * 
	 */
	function checkUser()
		{
		$dbconn = $this->getDBConnection();
	 	$sql = "SELECT Utenti.*, Config.*" .
	 			" FROM Utenti" .
	 			" LEFT JOIN Config ON Utenti.IDUtente=Config.IDUtente" .
	 			" WHERE Utenti.IDUtente=" . $dbconn->sqlInteger($_GET["u"]) .
	 			" AND Utenti.Sospeso<>1";
		$this->user = $this->getRecordset($sql, $dbconn, 1)->records[0];
		if (!$this->user)
			{
			$this->showMessage("Utente non definito", "Utente non definito", false, false);
			}
			
		}
		
	//***************************************************************************
	/**
	* manda in output la pagina
	* 
	*/
	function show()
		{
		if (!$this->view)
			{
			include_once __DIR__ . "/../ui/view/front/front.php";
			$this->view = new \webappointmentmanager\front\waapplication_view();
			}
			
		$this->addItem($this->user->CSS, "user_css");

		waLibs\waApplication::show();
		exit();
		}
		
//***************************************************************************
	} 	// fine classe front
	
//***************************************************************************

