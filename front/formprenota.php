<?php
include "front.inc.php";

//*****************************************************************************
class page extends front
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
	
	/**
	 *
	 * @var waLibs\waRecord
	 */
	var $appuntamento;
		
		
	//**************************************************************************
	function __construct()
		{
		parent::__construct();
		
		$this->appuntamento = $this->getAppuntamento();
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->updateRecord();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$context = $this->appuntamento->DescrizioneGiorno . " " . 
					date("d/m/Y", $this->appuntamento->DataAppuntamento) . 
				" ore " . date("H:i", $this->appuntamento->DalleOre); 
		$this->addItem("Prenotazione appuntamento\n$context", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		
		$this->form = new waLibs\waForm(null, $this);
		include_once __DIR__ . "/../ui/view/front/waform/waform.php";
		$this->form->view = new \webappointmentmanager\front\waFormView();
		
		$readOnly = false;
		
		$this->form->addText("Nome", "Cognome e nome", $readOnly, true);
		$this->form->addText("EMail", "Indirizzo e-mail", $readOnly, true);
		
		$this->form->addText("Telefono", "Telefono", $readOnly, $this->user->ObbligoTelefono);
		$this->form->addText("Via", "Via", $readOnly, $this->user->ObbligoIndirizzo);
		$this->form->addText("CAP", "CAP", $readOnly, $this->user->ObbligoIndirizzo)->maxChars = 5;
		$this->form->addText("Citta", "Città", $readOnly, $this->user->ObbligoIndirizzo);
		$this->form->addText("Provincia", "Provincia", $readOnly, $this->user->ObbligoIndirizzo)->maxChars = 2;
		$this->form->addTextArea("Note", "Note", $readOnly);
		
		$this->form->addTextArea("Testo196", "Informativa trattamento dati", true)
			->value = $this->get1962003();
		
		$this->form->addBoolean("ChkInformativa", "Ho letto l'informativa", false, true);
		
		$this->form->addCaptcha("Captcha", "Codice di controllo", false, true);

		new waLibs\waButton($this->form, 'cmd_submit', "Prenota");
		$cancelCtrl = new waLibs\waButton($this->form, 'cmd_cancel', "Annulla");
			$cancelCtrl->cancel = true;
			$cancelCtrl->submit = false;
		
		$this->form->getInputValues();

		}

	//*****************************************************************************
	function get1962003()
		{
		return str_replace("___", $this->user->NomeIndirizzo, file_get_contents("196.txt"));
		}
		
	//***************************************************************************
	/**
	* -
	*
	* @return waLibs\waRecord
	*/
	function getAppuntamento()
		{
		$dbconn = $this->getDBConnection();
		$sql = "SELECT Appuntamenti.*," .
				" Giorni.DescrizioneGiorno" .
				" FROM Appuntamenti" .
				" INNER JOIN Giorni ON DATE_FORMAT(Appuntamenti.DataAppuntamento, '%w')=Giorni.IDGiorno".
				" WHERE Appuntamenti.IDAppuntamento=" . $dbconn->sqlInteger($_GET["id"]) . 
				" AND Appuntamenti.IDUtente=" . $dbconn->sqlInteger($this->user->IDUtente) . 
				" AND Appuntamenti.Sospeso<>1"; 
		$record = $this->getRecordset($sql, $dbconn, 1)->records[0];
		if (!$record)
			{
			$this->showMessage("Nessun record", "Siamo spiacenti, l'appuntamento non è disponibile");
			}

		if ($record->IDStatoappuntamento != APPL_APP_LIBERO)
			{
			$this->showMessage("Non disponibile", "Siamo spiacenti, l'appuntamento non è più disponibile", true);
			}

		return $record;
		}
		
	//***************************************************************************
	function updateRecord()
		{
		$this->checkMandatory($this->form);
		
		$this->mandaMail();
		
		$this->showMessage("Prenotazione effettuata", 
				"La prima fase della prenotazione è terminata correttamente. " .
				"Ricevera' un messaggio di posta elettronica all'indirizzo da lei" . 
				" indicato (<b>$_POST[EMail]</b>): segua le indicazioni contenute nel " .
				"messaggio per completare la prenotazione.", false, false);
		}
		
	//***************************************************************************
	function mandaMail()
		{
		// salviamo i dati della prenotazione sotto forma di stringa encryptata,
		// la cui chiave conosce solo l'utente
		$qs = serialize(array	(
								'u' => $this->user->IDUtente, 
								'id' => $_GET['id'],
								'Nome' => $_POST['Nome'],
								'EMail' => $_POST['EMail'],
								'Telefono' => $_POST['Telefono'],
								'Via' => $_POST['Via'],
								'CAP' => $_POST['CAP'],
								'Citta' => $_POST['Citta'],
								'Provincia' => $_POST['Provincia'],
								'Note' => $_POST['Note']
								)
						);
					
		$esito = $this->saveEncryptString($qs);	
		if (!is_array($esito))
			{
			$this->showMessage("Errore generazione messaggio email", "Si è verificato un errore durante la generazione del messaggio di posta elettronica" .
					" da inviare all'indirizzo $_POST[EMail]. Si prega di avvertire l'assistenza tecnica o di riprovare più tardi.");
			}

		list($key, $filename) = $esito;
		$filename = basename($filename);
					
		$body = "Salve $_POST[Nome],\r\n\r\n" .
				"questo è un messaggio automatico generato dal servizio $this->title.\r\n\r\n" .
				"Ha effettuato una richiesta di prenotazione appuntamento a '" .
				$this->user->NomeIndirizzo . "'?" . 
				" Se si', il suo appuntamento:\r\n\r\n" .
				"in data:  " . $this->appuntamento->DescrizioneGiorno . " " .
						date('d/m/Y', $this->appuntamento->DataAppuntamento) . "\r\n" .
				"alle ore: " . date('H:i', $this->appuntamento->DalleOre) . "\r\n\r\n" .
				"deve essere confermato visitando la seguente pagina web:\r\n\r\n" .
				"$this->protocol://$this->domain$this->httpwd/front/confermaprenotazione.php" .
					"?u=" . $this->user->IDUtente . "&k=$key&f=$filename\r\n\r\n" .
				
				"Qualora invece lei non avesse effettuato alcuna richiesta di prenotazione, la preghiamo di scusarci dell'intrusione: " .
				"qualche buontempone ci ha fatto uno scherzo di pessimo gusto, usando sconsideratamente il suo indirizzo di posta elettronica." .
				" In questo caso, non deve fare assolutamente nulla: se non confermata, la prenotazione non avrà alcun effetto.\r\n\r\n" .
				"Arrivederci da $this->title ($this->protocol://$this->domain$this->httpwd).";
	
		if (!$this->sendMail($_POST['EMail'], "Conferma prenotazione - $this->title", $body, array($this->user->EMail, $this->tile)))
			{
			$this->showMessage("Errore invio messaggio email", "Si è verificato un errore durante l'invio del messaggio di posta elettronica" .
					" all'indirizzo $_POST[EMail]. Si prega di avvertire l'assistenza tecnica o di riprovare più tardi.");
			}
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
