<?php
//*****************************************************************************
include "webappointmentmanager.inc.php";

//*****************************************************************************
class page extends webappointmentmanager
	{
	
	//**************************************************************************
	function __construct()
		{
		parent::__construct(false);
		
		$page = base64_decode($_GET["p"]);
		$pageTitle = base64_decode($_GET["pt"]);
		$field = base64_decode($_GET["f"]);
		$fieldLabel = base64_decode($_GET["fl"]);
		
		$dbconn = $this->getDBConnection();
		$Sql = "SELECT Help.*" .
			" FROM Help" .
			" WHERE Help.Pagina=" . $dbconn->sqlString($page) .
			" AND NomeCampo=" . $dbconn->sqlString($field) .
			" AND NOT Sospeso";
		$help = $this->getRecordset($Sql, $dbconn, 1)->records[0];
		
		if (!$help)
			{
			$this->showMessage("Help non disponibile", "Help online non disponibile per l'elemento in esame", false, true);
			}

		$this->addItem($pageTitle . ($fieldLabel ? " - $fieldLabel" : ""), "title");
		$this->addItem(nl2br($help->Testo), "help_text");
		
		if ($_SERVER["QUERY_STRING"]) 
			{
			$genericHelpLink = "<a href='?'>Vai all'help generale dell'applicazione</a>";
			$this->addItem($genericHelpLink, "generic_help_link");
			}
		
		$form = $this->getForm();
		$ctrl = new waLibs\waButton($form, 'cmd_cancel', 'Chiudi');
		$ctrl->cancel = true;
		$this->addItem($form);
			
		$this->show();
		}
		
	}
	
//*****************************************************************************
// istanzia la pagina
new page();
