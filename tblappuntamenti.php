<?php
//******************************************************************************
include "webappointmentmanager.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends webappointmentmanager
	{
		
	//*****************************************************************************
	function __construct()
		{
		parent::__construct();

		if ($_POST["action"] == "check_exists")
			{
			// chiamata ajax per verificare l'esistenza di un appuntamento
			$this->rpc_checkExists();
			}
			
		$this->addItem($this->getMenu());
		
		$context = $_GET["flt"];
		$context = $_GET["flt"] == "validazione" ? "in attesa di validazione" : $context;
		$this->addItem("Appuntamenti $context", "title");
		$this->addItem($this->getTable());
		$this->show();
		}

	//*****************************************************************************
	/**
	 * @return waLibs\waTable
	 */
	function getTable()
		{
		// creazione della tabella
		$dbconn = $this->getDBConnection();
		$sql = "SELECT Appuntamenti.*," .
				" Giorni.DescrizioneGiorno," .
				" StatiAppuntamenti.DescrizioneStatoAppuntamento" .
				" FROM Appuntamenti" .
				" INNER JOIN Giorni ON DATE_FORMAT(Appuntamenti.DataAppuntamento, '%w')=Giorni.IDGiorno".
				" INNER JOIN StatiAppuntamenti ON Appuntamenti.IDStatoAppuntamento=StatiAppuntamenti.IDStatoAppuntamento" .
				" LEFT JOIN Config ON Appuntamenti.IDUtente=Config.IDUtente" .
				" WHERE NOT Appuntamenti.Sospeso" .
				" AND Appuntamenti.IDUtente=" . $dbconn->sqlInteger($this->user->IDUtente) .
				($_GET["flt"] == "archiviati" ? 
						" AND Appuntamenti.DataAppuntamento<ADDDATE(CURRENT_DATE(), IFNULL(Config.NrGiorniPrenotazione, 0))" . 
						" AND Appuntamenti.IDStatoAppuntamento!=" . $dbconn->sqlInteger(APPL_APP_ATTESA_VALIDAZIONE) : 
				'') .
				($_GET["flt"] == "liberi" ? 
					" AND Appuntamenti.IDStatoAppuntamento=" . $dbconn->sqlInteger(APPL_APP_LIBERO) .
					" AND Appuntamenti.DataAppuntamento>=ADDDATE(CURRENT_DATE(), IFNULL(Config.NrGiorniPrenotazione, 0))" : 
				'') .
				($_GET["flt"] == "prenotati" ? 
					" AND Appuntamenti.IDStatoAppuntamento=" . $dbconn->sqlInteger(APPL_APP_PRENOTATO) .
					" AND Appuntamenti.DataAppuntamento>=CURRENT_DATE()" : 
				'') .
				($_GET["flt"] == "validazione" ? 
					" AND Appuntamenti.IDStatoAppuntamento=" . $dbconn->sqlInteger(APPL_APP_ATTESA_VALIDAZIONE) : 
				'') .
				" ORDER BY Appuntamenti.DataAppuntamento, Appuntamenti.DalleOre";
		
		$table = parent::getTable($sql);
		
		if ($_GET["flt"]  == "validazione")
			{
			$this->setTableValidazione($table);
			}
		elseif ($_GET["flt"] == "archiviati")
			{
			$this->setTableArchiviati($table);
			}
		else
			{
			$this->setTableEdit($table);
			}

		// lettura dal database delle righe che andranno a popolare la tabella
		if (!$table->loadRows())
			{
			$this->showDBError($table->recordset->dbConnection);
			}

		return $table;
		}

	//*****************************************************************************
	function setTableEdit(waLibs\waTable_quickedit $table)
		{
		$table->addColumn("IDAppuntamento", "ID", false, false, false)->aliasOf = "Appuntamenti.IDAppuntamento";
		
		$col = $table->addColumn("DescrizioneGiorno", "Giornata");
			$col->aliasOf = "Giorni.DescrizioneGiorno";
			$col->alignment = waLibs\waTable::ALIGN_C;
			
		$col = $table->addColumn("DataAppuntamento", "Data");
			$col->aliasOf = "Appuntamenti.DataAppuntamento";
			$col->inputType = waLibs\waTable::INPUT_DATE;
			$col->inputMandatory = true;
		
		$col = $table->addColumn("DalleOre", "Dalle ore");
			$col->aliasOf = "Appuntamenti.DalleOre";
			$col->format = waLibs\waTable::FMT_TIME;
			$col->inputType = waLibs\waTable::INPUT_TIME;
			$col->inputMandatory = true;
			
		$col = $table->addColumn("AlleOre", "Alle ore");
			$col->aliasOf = "Appuntamenti.AlleOre";
			$col->format = waLibs\waTable::FMT_TIME;
			$col->inputType = waLibs\waTable::INPUT_TIME;
			$col->inputMandatory = true;
		
		if (!$_GET["flt"] || $_GET["flt"] == "archiviati")
			{
			if ($table->isExport())
				{
				$col = $table->addColumn("DescrizioneStatoAppuntamento", "Stato");
				}
			else
				{
				$col = $table->addColumn("IDStatoAppuntamento", "Stato");
				$col->inputType = waLibs\waTable::INPUT_SELECT;
				$col->inputMandatory = true;
				$sql = "SELECT IDStatoAppuntamento, DescrizioneStatoAppuntamento" .
						" FROM StatiAppuntamenti" .
						" ORDER BY IDStatoAppuntamento";
				$col->inputOptions[''] = '';
				foreach ($this->getRecordset($sql)->records as $record)
					{
					$col->inputOptions[$record->IDStatoAppuntamento] = $record->DescrizioneStatoAppuntamento;
					}
				}
			$col->aliasOf = "StatiAppuntamenti.DescrizioneStatoAppuntamento";
			$col->alignment = waLibs\waTable::ALIGN_C;
			}
			
		$col = $table->addColumn("NoteAppuntamento", "Note");
			$col->aliasOf = "Appuntamenti.NoteAppuntamento";
			$col->inputType = waLibs\waTable::INPUT_TEXTAREA;
			
		// verifica che non sia stato richiesto un eventuale input dati
		$table->getInputValues ();
		if ($table->isToUpdate())
			{
			if (!$table->record->IDUtente)
				{
				// è un inserimento
				$table->record->IDUtente = $this->user->IDUtente;
				}
			elseif ($table->record->IDUtente != $this->user->IDUtente)
				{
				// frode? ma dai!!!
				$table->RPCResponse (waLibs\waTable::RPC_KO, "db error");
				}
				
			if (!$table->record->IDStatoAppuntamento)
				{
				$table->record->IDStatoAppuntamento = $_GET["flt"] == "prenotati" ? APPL_APP_PRENOTATO : APPL_APP_LIBERO;
				}
				
			$this->setEditorData($table->record);
			$table->save();
			}

		}
		
	//*****************************************************************************
	function setTableArchiviati(waLibs\waTable_quickedit $table)
		{
		$table->removeAction("New");
		
		$table->addColumn("IDAppuntamento", "ID", false, false, false)->aliasOf = "Appuntamenti.IDAppuntamento";
		
		$col = $table->addColumn("DescrizioneGiorno", "Giornata");
			$col->aliasOf = "Giorni.DescrizioneGiorno";
			$col->alignment = waLibs\waTable::ALIGN_C;
			
		$col = $table->addColumn("DataAppuntamento", "Data");
			$col->aliasOf = "Appuntamenti.DataAppuntamento";
		
		$col = $table->addColumn("DalleOre", "Dalle ore");
			$col->aliasOf = "Appuntamenti.DalleOre";
			$col->format = waLibs\waTable::FMT_TIME;
			
		$col = $table->addColumn("AlleOre", "Alle ore");
			$col->aliasOf = "Appuntamenti.AlleOre";
			$col->format = waLibs\waTable::FMT_TIME;

		$col = $table->addColumn("DescrizioneStatoAppuntamento", "Stato");
		$col->alignment = waLibs\waTable::ALIGN_C;
			
		$col = $table->addColumn("NoteAppuntamento", "Note");
			$col->aliasOf = "Appuntamenti.NoteAppuntamento";
			
		// verifica che non sia stato richiesto un eventuale input dati
		$table->getInputValues ();
		if ($table->isToUpdate())
			{
			if ($table->record->IDUtente != $this->user->IDUtente)
				{
				// frode? ma dai!!!
				$table->RPCResponse (waLibs\waTable::RPC_KO, "db error");
				}
			$table->save();
			}

		}
		
	//*****************************************************************************
	function setTableValidazione(waLibs\waTable_quickedit $table)
		{
		include_once __DIR__ . "/ui/view/watable/watable_validazione.php";
		$table->view = new \webappointmentmanager\watable_validazione_view();

		$table->listMaxRec = 0;
		$table->removeAction("New");
		$table->removeAction("no_pagination");
		$table->removeAction("Filter");
		$table->addAction("NewPeriod", false, "Crea");
		$table->addAction("Validate", false, "Valida");
		
		$table->addColumn("IDAppuntamento", "ID", false, false, false)->aliasOf = "Appuntamenti.IDAppuntamento";
		
		$col = $table->addColumn("DescrizioneGiorno", "Giornata");
			$col->aliasOf = "Giorni.DescrizioneGiorno";
			$col->alignment = waLibs\waTable::ALIGN_C;
		
		$table->addColumn("DataAppuntamento", "Data")->aliasOf = "Appuntamenti.DataAppuntamento";
		
		$col = $table->addColumn("DalleOre", "Dalle ore");
			$col->aliasOf = "Appuntamenti.DalleOre";
			$col->format = waLibs\waTable::FMT_TIME;
			
		$col = $table->addColumn("AlleOre", "Alle ore");
			$col->aliasOf = "Appuntamenti.AlleOre";
			$col->format = waLibs\waTable::FMT_TIME;

		// verifica che non sia stato richiesto un eventuale input dati
		$table->getInputValues ();
		if ($table->isToUpdate())
			{
			if ($table->record->IDUtente != $this->user->IDUtente)
				{
				// frode? ma dai!!!
				$table->RPCResponse (waLibs\waTable::RPC_KO, "db error");
				}
			$table->save();
			}

		}
		
	//*****************************************************************************
	//*****   funzioni rpc   ******************************************************
	//*****************************************************************************
	function rpc_checkExists()
		{
		
		$dbconn = $this->getDBConnection();
		$sql = "SELECT * FROM Appuntamenti" .
				" WHERE IDUtente=" . $dbconn->sqlInteger($this->user->IDUtente) .
				" AND DataAppuntamento=" . $dbconn->sqlDate($_POST["DataAppuntamento"]) .
				" AND AlleOre>=" . $dbconn->sqlTime($_POST["DalleOre"]) .
				" AND DalleOre<=" . $dbconn->sqlTime($_POST["AlleOre"]) .
				" AND IDAppuntamento!=" . $dbconn->sqlInteger($_POST["IDAppuntamento"]) .
				" AND Sospeso<>1";
		
		$retval["esito"] = $this->getRecordset($sql, $dbconn, 1)->records[0] ? 1 : 0;
		$this->rpcResponse($retval);
		
		}
		
	//*****************************************************************************
	}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new page();