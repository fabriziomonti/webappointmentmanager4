<?php
//******************************************************************************
include "webappointmentmanager.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends webappointmentmanager
	{
		
	//*****************************************************************************
	function __construct()
		{
		parent::__construct();

		if ($_POST["action"] == "check_exists")
			{
			// chiamata ajax per verificare l'esistenza di un orario di ricevimento
			$this->rpc_checkExists();
			}
			
		$this->addItem($this->getMenu());
		
		$this->addItem("Orari di ricevimento", "title");
		$this->addItem($this->getTable());
		$this->show();
		}

	//*****************************************************************************
	/**
	 * @return waLibs\waTable
	 */
	function getTable()
		{
		// creazione della tabella
		$dbconn = $this->getDBConnection();
		$sql = "SELECT OrariRicevimento.*," .
				" Giorni.DescrizioneGiorno" .
				" FROM OrariRicevimento" .
				" INNER JOIN Giorni ON OrariRicevimento.IDGiorno=Giorni.IDGiorno" .
				" WHERE NOT OrariRicevimento.Sospeso" .
				" AND OrariRicevimento.IDUtente=" . $dbconn->sqlInteger($this->user->IDUtente) .
				"  ORDER BY OrariRicevimento.IDGiorno, OrariRicevimento.DalleOre";
		$table = parent::getTable($sql);
		
		$table->addColumn("IDOrarioRicevimento", "ID", false, false, false)->aliasOf = "OrariRicevimento.IDOrarioRicevimento";
		
		if ($table->isExport())
			{
			$col = $table->addColumn("DescrizioneGiorno", "Giornata");
			}
		else
			{
			$col = $table->addColumn("IDGiorno", "Giornata");
			$col->inputType = waLibs\waTable::INPUT_SELECT;
			$col->inputMandatory = true;
			$sql = "SELECT IDGiorno, DescrizioneGiorno" .
					" FROM Giorni" .
					" ORDER BY IDGiorno";
			$col->inputOptions[''] = '';
			foreach ($this->getRecordset($sql)->records as $record)
				{
				$col->inputOptions[$record->IDGiorno] = $record->DescrizioneGiorno;
				}
			}
		$col->aliasOf = "Giorni.DescrizioneGiorno";
		$col->alignment = waLibs\waTable::ALIGN_C;
			
		$col = $table->addColumn("DalleOre", "Dalle ore");
			$col->aliasOf = "OrariRicevimento.DalleOre";
			$col->format = waLibs\waTable::FMT_TIME;
			$col->inputType = waLibs\waTable::INPUT_TIME;
			$col->inputMandatory = true;
			
		$col = $table->addColumn("AlleOre", "Alle ore");
			$col->aliasOf = "OrariRicevimento.AlleOre";
			$col->format = waLibs\waTable::FMT_TIME;
			$col->inputType = waLibs\waTable::INPUT_TIME;
			$col->inputMandatory = true;
			
		$col = $table->addColumn("NoteOrarioRicevimento", "Note");
			$col->aliasOf = "OrariRicevimento.NoteOrarioRicevimento";
			$col->inputType = waLibs\waTable::INPUT_TEXTAREA;

		// verifica che non sia stato richiesto un eventuale input dati
		$table->getInputValues ();
		if ($table->isToUpdate())
			{
			if (!$table->record->IDUtente)
				{
				// è un inserimento
				$table->record->IDUtente = $this->user->IDUtente;
				}
			elseif ($table->record->IDUtente != $this->user->IDUtente)
				{
				// frode? ma dai!!!
				$table->RPCResponse (waLibs\waTable::RPC_KO, "db error");
				}
				
			$this->setEditorData($table->record);
			$table->save();
			}

		// lettura dal database delle righe che andranno a popolare la tabella
		if (!$table->loadRows())
			{
			$this->showDBError($table->recordset->dbConnection);
			}

		return $table;
		}

	//*****************************************************************************
	//*****   funzioni rpc   ******************************************************
	//*****************************************************************************
	function rpc_checkExists()
		{
		$dbconn = $this->getDBConnection();
		$sql = "SELECT * FROM OrariRicevimento" .
				" WHERE IDUtente=" . $dbconn->sqlInteger($this->user->IDUtente) .
				" AND IDGiorno=" . $dbconn->sqlInteger($_POST["IDGiorno"]) .
				" AND AlleOre>=" . $dbconn->sqlTime($_POST["DalleOre"]) .
				" AND DalleOre<=" . $dbconn->sqlTime($_POST["AlleOre"]) .
				" AND IDOrarioRicevimento!=" . $dbconn->sqlInteger($_POST["IDOrarioRicevimento"]) .
				" AND Sospeso<>1";

		$retval["esito"] = $this->getRecordset($sql, $dbconn, 1)->records[0] ? 1 : 0;
		$this->rpcResponse($retval);
		}
		
	//*****************************************************************************
	}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new page();