SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS Appuntamenti (
IDAppuntamento int(10) NOT NULL,
  IDUtente int(10) NOT NULL DEFAULT '0',
  DataAppuntamento date NOT NULL DEFAULT '0000-00-00',
  DalleOre datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  AlleOre datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  IDStatoAppuntamento int(10) NOT NULL DEFAULT '0',
  NoteAppuntamento text COLLATE latin1_general_ci,
  Sospeso int(1) DEFAULT '0',
  DataOraUltimaModifica datetime DEFAULT NULL,
  IPUltimaModifica varchar(20) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE IF NOT EXISTS Config (
IDConfig int(10) NOT NULL,
  IDUtente int(10) NOT NULL DEFAULT '0',
  DurataMediaMinuti int(4) NOT NULL DEFAULT '0',
  MinutiPausa int(4) NOT NULL DEFAULT '0',
  NrGiorniPrenotazione int(4) NOT NULL DEFAULT '0',
  ObbligoTelefono int(1) NOT NULL DEFAULT '0',
  ObbligoIndirizzo int(1) NOT NULL DEFAULT '0',
  ConfermaAutomatica int(1) NOT NULL DEFAULT '0',
  NoteConfig text COLLATE latin1_general_ci,
  Sospeso int(1) DEFAULT '0',
  DataOraUltimaModifica datetime DEFAULT NULL,
  IPUltimaModifica varchar(20) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE IF NOT EXISTS Giorni (
  IDGiorno int(1) NOT NULL DEFAULT '0',
  DescrizioneGiorno varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  Sospeso int(1) DEFAULT '0',
  DataOraUltimaModifica datetime DEFAULT NULL,
  IPUltimaModifica varchar(20) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE IF NOT EXISTS `Help` (
IDHelp int(10) NOT NULL,
  Pagina varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  Filtro varchar(20) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  IDOperazione int(10) NOT NULL DEFAULT '0',
  NomeCampo varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  Testo longtext COLLATE latin1_general_ci NOT NULL,
  NoteHelp text COLLATE latin1_general_ci,
  Sospeso int(1) DEFAULT '0',
  DataOraUltimaModifica datetime DEFAULT NULL,
  IPUltimaModifica varchar(20) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE IF NOT EXISTS Operazioni (
  IDOperazione int(10) NOT NULL DEFAULT '0',
  DescrizioneOperazione varchar(40) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  NoteOperazione text COLLATE latin1_general_ci,
  Sospeso int(1) DEFAULT '0',
  DataOraUltimaModifica datetime DEFAULT '0000-00-00 00:00:00',
  IPUltimaModifica varchar(20) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE IF NOT EXISTS OrariRicevimento (
IDOrarioRicevimento int(10) NOT NULL,
  IDUtente int(10) NOT NULL DEFAULT '0',
  IDGiorno int(1) NOT NULL DEFAULT '0',
  DalleOre datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  AlleOre datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  NoteOrarioRicevimento text COLLATE latin1_general_ci,
  Sospeso int(1) DEFAULT '0',
  DataOraUltimaModifica datetime DEFAULT NULL,
  IPUltimaModifica varchar(20) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE IF NOT EXISTS StatiAppuntamenti (
  IDStatoAppuntamento int(10) NOT NULL DEFAULT '0',
  DescrizioneStatoAppuntamento varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  NoteStatoAppuntamento text COLLATE latin1_general_ci,
  Sospeso int(1) DEFAULT '0',
  DataOraUltimaModifica datetime DEFAULT NULL,
  IPUltimaModifica varchar(20) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE IF NOT EXISTS Utenti (
IDUtente int(10) NOT NULL,
  Login varchar(10) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `Password` varchar(10) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  Email varchar(255) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  NomeIndirizzo varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  CSS varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  CreateDate datetime DEFAULT NULL,
  FirstEMail varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  NoteUtente text COLLATE latin1_general_ci,
  Sospeso int(1) DEFAULT '0',
  DataOraUltimaModifica datetime DEFAULT NULL,
  IPUltimaModifica varchar(20) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;


ALTER TABLE Appuntamenti
 ADD PRIMARY KEY (IDAppuntamento);

ALTER TABLE Config
 ADD PRIMARY KEY (IDConfig), ADD KEY IDUtente (IDUtente);

ALTER TABLE Giorni
 ADD UNIQUE KEY IDGiorno (IDGiorno);

ALTER TABLE Help
 ADD PRIMARY KEY (IDHelp), ADD KEY URI (Pagina), ADD KEY NomeCampo (NomeCampo);

ALTER TABLE Operazioni
 ADD PRIMARY KEY (IDOperazione);

ALTER TABLE OrariRicevimento
 ADD PRIMARY KEY (IDOrarioRicevimento), ADD KEY IDUtente (IDUtente);

ALTER TABLE StatiAppuntamenti
 ADD PRIMARY KEY (IDStatoAppuntamento);

ALTER TABLE Utenti
 ADD PRIMARY KEY (IDUtente), ADD KEY Login (Login);


ALTER TABLE Appuntamenti
MODIFY IDAppuntamento int(10) NOT NULL AUTO_INCREMENT;
ALTER TABLE Config
MODIFY IDConfig int(10) NOT NULL AUTO_INCREMENT;
ALTER TABLE Help
MODIFY IDHelp int(10) NOT NULL AUTO_INCREMENT;
ALTER TABLE OrariRicevimento
MODIFY IDOrarioRicevimento int(10) NOT NULL AUTO_INCREMENT;
ALTER TABLE Utenti
MODIFY IDUtente int(10) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
