<?php
//*****************************************************************************
include "webappointmentmanager.inc.php";

//*****************************************************************************
class page extends webappointmentmanager
	{
	
	//**************************************************************************
	function __construct()
		{
		parent::__construct();
		
		$dbconn = $this->getDBConnection();
		$sql = "UPDATE Appuntamenti SET IDStatoAppuntamento=" . $dbconn->sqlInteger(APPL_APP_LIBERO) .
				" WHERE Appuntamenti.IDUtente=" . $dbconn->sqlInteger($this->user->IDUtente) .
				" AND Appuntamenti.IDStatoAppuntamento=" . $dbconn->sqlInteger(APPL_APP_ATTESA_VALIDAZIONE) . 
				" AND Appuntamenti.Sospeso<>1" ;
		
		$this->dbExecute($sql, $dbconn);
		$this->response();
		}
	
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
