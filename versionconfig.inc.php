<?php
if (!defined('__VERSIONCONFIG_VARS'))
{
	define('__VERSIONCONFIG_VARS',1);
	
	define('APPL_REL', 							'4.0.4');
	define('APPL_REL_DATE', 					mktime(0,0,0, 11, 11, 2016));

	
} //  if (!defined('__VERSIONCONFIG_VARS'))
