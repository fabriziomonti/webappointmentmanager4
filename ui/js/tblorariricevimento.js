//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe waPage
var waPage = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: webappointmentmanager,

	//-------------------------------------------------------------------------
	// proprieta'

	//-------------------------------------------------------------------------
	action_waTable_quickEdit_DalleOre: function(rowId)
		{
		var proxy = this;
		setTimeout(function() {proxy.verificaOra(rowId)}, 0);
		return this.table.action_quickEdit("DalleOre", rowId);
		},
		
	//-------------------------------------------------------------------------
	action_waTable_quickEdit_AlleOre: function(rowId)
		{
		var proxy = this;
		setTimeout(function() {proxy.verificaOra(rowId)}, 0);
		return this.table.action_quickEdit("AlleOre", rowId);
		},
		
	//-------------------------------------------------------------------------
	verificaOra: function(rowId)
		{
		var DalleOre = this.table.columns["DalleOre"].getInputValue(rowId);
		if (!DalleOre)
			return;
		
		var AlleOre = this.table.columns["AlleOre"].getInputValue(rowId);
		if (!AlleOre)
			return;

		if (DalleOre > AlleOre)
			{
			this.alert("Orario di inizio maggiore dell'orario di fine");	
			return;
			}
			
		this.verificaEsistenza(rowId);
		},
		
	//-------------------------------------------------------------------------
	verificaEsistenza: function(rowId)
		{
		var DalleOre = this.table.columns["DalleOre"].getInputValue(rowId);
		if (!DalleOre)
			return;
		
		var AlleOre = this.table.columns["AlleOre"].getInputValue(rowId);
		if (!AlleOre)
			return;

		var IDGiorno = this.table.columns["IDGiorno"].getInputValue(rowId);
		if (!IDGiorno)
			return;
		
		var post = {};
		post.action = 'check_exists';
		post.IDGiorno = IDGiorno;
		post.DalleOre = DalleOre / 1000;
		post.AlleOre = AlleOre / 1000;
		post.IDOrarioRicevimento = rowId;
		
		jQuery.ajax
			(
				{
				url			: document.location.href,
				type		: 'POST',
				dataType	: 'json',
				data		: post,
				timeout		: 20000,
				success		: function (response, textStatus, jqXHR) 
								{
								if (response.esito)
									{
									this.alert("Orario gia' presente in archivio");	
									}
								},
				error		: function(jqXHR, textStatus, errorThrown)
								{
								}
				}
			);
	
		}
		
	}
);

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
document.waPage = new waPage();
