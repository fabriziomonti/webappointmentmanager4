//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe waPage
var waPage = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: webappointmentmanager,

	//-------------------------------------------------------------------------
	// proprieta'

	//-------------------------------------------------------------------------
	action_waTable_quickDelete: function(rowId)
		{
		if (jQuery("table td textarea").length)
			{
			// laddove c'è possibilità di editing chiediamo conferma della 
			// cancellazione
			this.confirm("Confermi Cancellazione?", function() {document.waPage.gotDeleteConfirm(rowId);});
			}
		else
			{
			this.gotDeleteConfirm(rowId);
			}
		},
		
	//-------------------------------------------------------------------------
	gotDeleteConfirm: function(rowId)
		{
		this.table.RPC(this.table.gotQuickDeleteResponse, "watable_rpc_quick_delete", rowId);
		},
		
	//-------------------------------------------------------------------------
	action_waTable_Validate: function()
		{
		this.confirm("Confermi validazione appuntamenti?", function() {document.waPage.gotValidateConfirm();});
		},
		
	//-------------------------------------------------------------------------
	gotValidateConfirm: function()
		{
		this.openPage("validazione.php");
		},
		
	//-------------------------------------------------------------------------
	action_waTable_NewPeriod: function()
		{
		this.openPage("formnewperiod.php");
		},
		
	//-------------------------------------------------------------------------
	action_waTable_quickEdit_DataAppuntamento: function(rowId)
		{
		var col =  this.table.columns["DataAppuntamento"];
		if (col.verifyInput(rowId) != '')
			{
			this.alert("Campo obbligatorio o formato data non valido (gg/mm/aaaa)");
			return false;
			}
			
		var value = this.table.columns["DataAppuntamento"].getInputValue(rowId);
		if (value)
			{
			var day = (new Date(value)).getDay();
			var days = ["Domenica", "Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato"];
			this.table.rows[rowId].setCellContent("DescrizioneGiorno", days[day]);
			}

		this.verificaEsistenza(rowId);
		return this.table.action_quickEdit("DataAppuntamento", rowId);
		},
		
	//-------------------------------------------------------------------------
	action_waTable_quickEdit_DalleOre: function(rowId)
		{
		var proxy = this;
		setTimeout(function() {proxy.verificaOra(rowId)}, 0);
		return this.table.action_quickEdit("DalleOre", rowId);
		},
		
	//-------------------------------------------------------------------------
	action_waTable_quickEdit_AlleOre: function(rowId)
		{
		var proxy = this;
		setTimeout(function() {proxy.verificaOra(rowId)}, 0);
		return this.table.action_quickEdit("AlleOre", rowId);
		},
		
	//-------------------------------------------------------------------------
	verificaOra: function(rowId)
		{
		var DalleOre = this.table.columns["DalleOre"].getInputValue(rowId);
		if (!DalleOre)
			return;
		
		var AlleOre = this.table.columns["AlleOre"].getInputValue(rowId);
		if (!AlleOre)
			return;

		if (DalleOre > AlleOre)
			{
			this.alert("Orario di inizio maggiore dell'orario di fine");	
			return;
			}
			
		this.verificaEsistenza(rowId);
		},
		
	//-------------------------------------------------------------------------
	verificaEsistenza: function(rowId)
		{
		var DalleOre = this.table.columns["DalleOre"].getInputValue(rowId);
		if (!DalleOre)
			return;
		
		var AlleOre = this.table.columns["AlleOre"].getInputValue(rowId);
		if (!AlleOre)
			return;

		var DataAppuntamento = this.table.columns["DataAppuntamento"].getInputValue(rowId);
		if (!DataAppuntamento)
			return;
		
		var post = {};
		post.action = 'check_exists';
		post.DataAppuntamento = DataAppuntamento / 1000;
		post.DalleOre = DalleOre / 1000;
		post.AlleOre = AlleOre / 1000;
		post.IDAppuntamento = rowId;
		
		jQuery.ajax
			(
				{
				url			: document.location.href,
				type		: 'POST',
				dataType	: 'json',
				data		: post,
				timeout		: 20000,
				success		: function (response, textStatus, jqXHR) 
								{
								if (response.esito)
									{
									this.alert("Orario gia' presente in archivio");	
									}
								},
				error		: function(jqXHR, textStatus, errorThrown)
								{
								}
				}
			);
	
		}
		
	}
);

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
document.waPage = new waPage();
