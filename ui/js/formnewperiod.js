//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe waPage
var waPage = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: webappointmentmanager,

	//-------------------------------------------------------------------------
	// proprieta'

	//-------------------------------------------------------------------------
	//initialization  
	// la fa il parent
	
	//-------------------------------------------------------------------------
	event_onchange_waForm_DallaData: function()
		{
		var DallaData = this.form.controls.DallaData.get();;
		if (!DallaData)
			return true;
		
		if (DallaData < (new Date()))
			{
			this.alert("Data di inizio periodo minore della data corrente");	
			this.form.controls.DallaData.set(null);
			return false;
			}

		if (!this.verifica())
			this.form.controls.DallaData.set(null);
		},
		
	//-------------------------------------------------------------------------
	event_onchange_waForm_AllaData: function()
		{
		var AllaData = this.form.controls.AllaData.get();;
		if (!AllaData)
			return true;
		
		var threeMonthAfter = new Date();
		threeMonthAfter.setMonth(threeMonthAfter.getMonth() + 3);
		if (AllaData > threeMonthAfter)
			{
			this.alert("Data di fine periodo posteriore a tre mesi dalla data corrente");	
			this.form.controls.AllaData.set(null);
			return false;
			}


		if (!this.verifica())
			this.form.controls.AllaData.set(null);
		},
		
	//-------------------------------------------------------------------------
	verifica: function()
		{
		var DallaData = this.form.controls.DallaData.get();;
		var AllaData = this.form.controls.AllaData.get();;

		if (!DallaData || !AllaData)
			return true;
		
		if (DallaData >= AllaData)
			{
			this.alert("Data di inizio maggiore della data di fine");	
			return false;
			}
			
		var esito = this.form.RPC("rpc_checkExists", DallaData / 1000, AllaData / 1000);
		if (esito)
			{
			this.alert("ATTENZIONE: esistono gia' appuntamenti in archivio per il periodo selezionato!");	
			return false;
			}
			
			
		return true;
		}

	
	}
);

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
document.waPage = new waPage();
