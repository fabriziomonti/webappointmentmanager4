<?php 
namespace webappointmentmanager\front;
use stdClass;

//******************************************************************************
class waTable_view implements \i_waTableView
	{
	
	/**
	 * dati in input
	 * @var stdClass
	 */
	protected $data = null;

	//**************************************************************************
	public function transform(stdClass $data)
		{
		$this->data = $data;

		$this->setCssLink();
		$this->setJavascriptLink();
		$this->setNavbar();
		$this->setTable();
		$this->setNavbar();
		$this->setJavascriptObjects();
		
		}
		
	//**************************************************************************
	protected function setCssLink()
		{
		?>

		<link href='<?=$this->data->watablePath?>/uis/watable_default/css/watable.css' rel='stylesheet'/>

		<?php
		}
		
	//**************************************************************************
	protected function setJavascriptLink()
		{
		?>
		<!-- Bootstrap core JavaScript
		================================================== -->
		
		<script type='text/javascript' src='<?=$this->data->watablePath?>/uis/watable_default/js/strmanage.js'></script>
		<script type='text/javascript' src='<?=$this->data->watablePath?>/uis/watable_default/js/wacolumn.js'></script>
		<script type='text/javascript' src='<?=$this->data->watablePath?>/uis/watable_default/js/warow.js'></script>
		<script type='text/javascript' src='<?=$this->data->watablePath?>/uis/watable_default/js/watable.js'></script>
		
		<?php
		}
		
	//**************************************************************************
	protected function setNavbar()
		{
		$tblName = $this->data->name;
		
		$first = $this->data->navbar->currentPageNr - 2 < 0 ? 0 : $this->data->navbar->currentPageNr - 2;
		$last = $first + 4 < $this->data->navbar->totalPageNr ? $first + 4 : $this->data->navbar->totalPageNr - 1;
		
		?>
		<div class='row watable_navbar'>

			<ul class="pagination">

				<?php
				$class = $this->data->navbar->currentPageNr == 0 ? "class='disabled'" : "";
				$href = $this->data->navbar->currentPageNr == 0 ? 
							"void(0)" : 
							"document.$tblName.goToPage(\"" . ($this->data->navbar->currentPageNr - 1) . '")';
				?>
				<li <?=$class?>>
					<a href='javascript:document.<?=$tblName?>.goToPage("0")'>&laquo;&laquo;</a>
				</li>
				<li <?=$class?>>
					<a href='javascript:<?=$href?>'>&laquo;</a>
				</li>

				<?php
				for ($li = $first; $li <= $last; $li++)
					{
					?>
					<li <?=$li == $this->data->navbar->currentPageNr ? "class='active'" : ""?>>
						<a href='javascript:document.<?=$this->data->name?>.goToPage("<?= $li ?>")'><?=$li + 1?></a></li>
					<?php
					}
					
				$class = $this->data->navbar->currentPageNr >= $this->data->navbar->totalPageNr - 1 ? "class='disabled'" : "";
				$href = $this->data->navbar->currentPageNr == $this->data->navbar->totalPageNr - 1 ? 
							"void(0)" : 
							"document." . $this->data->name . '.goToPage("' . ($this->data->navbar->currentPageNr + 1) . '")';
				?>
				<li <?=$class?>>
					<a href='javascript:<?=$href?>'>&raquo;</a>
				</li>
				<li <?=$class?>>
					<a href='javascript:document.<?=$tblName?>.goToPage("<?=$this->data->navbar->totalPageNr - 1?>")'>&raquo;&raquo;</a>
				</li>
			</ul>			
			
		</div>
		<?php
		}
		
	//**************************************************************************
	protected function setTable()
		{
		?>
		<form id='<?=$this->data->name?>' action='' method='post' class='watable'>
			<div class="table-responsive"> 
				<table class='table table-bordered table-striped table-hover'>
					<?php
					$this->setHeaders();
					$this->setRows();
					?>
				</table>
			</div>
		</form>
			
		<?php
		}
		
	//**************************************************************************
	protected function setRowActions($row)
		{
		$tblName = $this->data->name;

		?>
		<!--colonna delle azioni su record--> 
		<td style='width: 1%'>
			<div class='group-btn'>
			<?php
			foreach ($row->enableableActions as $idx => $action)
				{
				if (!$action->enable) continue
				?>
				<button  
					type='button' 
					class='btn'
					onclick='document.<?=$tblName?>.action_<?=$tblName?>_<?=$this->data->recordActions[$idx]->name?>("<?=$row->id?>")'
				>
					<?=$this->data->recordActions[$idx]->label?>
				</button>
				<?php
				}
			?>
			</div>
		</td>
		<?php
		}
		
	//**************************************************************************
	protected function setRows()
		{
		$tblName = $this->data->name;

		?>
		<tbody>
			<?php
			foreach ($this->data->rows as $row)
				{
				?>
				<tr id='row_<?=$tblName?>_<?=$row->id?>'>
					
					<?php
					$this->setRowData($row);
					$this->setRowActions($row);
					?>
					
				</tr>
				<?php
				}
			?>
		</tbody>
		<?php
		}
		
	//**************************************************************************
	protected function setRowData($row)
		{

		foreach ($row->cells as $idx => $cell)
			{
			$this->setCell($row, $cell, $idx);
			}
			
		}
		
	//**************************************************************************
	protected function setCell($row, $cell, $idx)
		{
		$tblName = $this->data->name;

		$hdr = $this->data->columnHeaders[$idx];
		if (!$hdr->show)
			{
			return;
			}

		?>
		<td style='text-align: <?=$this->getAlignment($hdr)?>' class='<?=$idx == 0 ? "id_cell" : "" ?>'>
		<?php

		if ($hdr->link)
			{
			?>
			<a href='javascript:document.<?=$tblName?>.link_<?=$tblName?>_<?=$hdr->name?>("<?=$row->id?>")'>
				<?=htmlspecialchars($cell->value)?>
			</a>
			<?php
			}

		elseif (!$hdr->HTMLConversion)
			{
			echo $cell->value;
			}
			
		// precedenza al formato definito esplicitamente
		elseif ($hdr->format == \waLibs\waTable::FMT_DATE)
			{
			if (strlen($cell->value))
				{
				echo substr($cell->value, 8, 2) . "/" .
						substr($cell->value, 5, 2) . "/" .
						substr($cell->value, 0, 4);
				}
			}

		elseif ($hdr->format == \waLibs\waTable::FMT_DATETIME)
			{
			if (strlen($cell->value))
				{
				echo substr($cell->value, 8, 2) . "/" .
								substr($cell->value, 5, 2) . "/" .
								substr($cell->value, 0, 4) . " " .
								substr($cell->value, 11, 2) . ":" .
								substr($cell->value, 14, 2);
				}
			}

		elseif ($hdr->format == \waLibs\waTable::FMT_TIME)
			{
			if (strlen($cell->value))
				{
				echo substr($cell->value, 0, 2) . ":" . substr($cell->value, 3, 2);
				}
			}

		elseif ($hdr->format == \waLibs\waTable::FMT_DECIMAL)
			{
			if (strlen($cell->value))
				{
				echo number_format($cell->value, 2, ",", ".");
				}
			}

//		elseif ($hdr->format == \waLibs\waTable::FMT_INTEGER)
//				{
//				if (strlen($cell->value))
//					echo number_format ($cell->value, 0, ",", ".");
//				}
			
		elseif ($hdr->format == \waLibs\waTable::FMT_STRING)
			{
			echo nl2br(htmlspecialchars($cell->value));
			}
			
		// default sul tipo campo
		elseif ($hdr->fieldType == \waLibs\waDB::DATE)
			{
			if (strlen($cell->value))
				{
				echo substr($cell->value, 8, 2) . "/" .
						substr($cell->value, 5, 2) . "/" .
						substr($cell->value, 0, 4);
				}
			}

		elseif ($hdr->fieldType == \waLibs\waDB::DATETIME)
			{
			if (strlen($cell->value))
				{
				echo substr($cell->value, 8, 2) . "/" .
								substr($cell->value, 5, 2) . "/" .
								substr($cell->value, 0, 4) . " " .
								substr($cell->value, 11, 2) . ":" .
								substr($cell->value, 14, 2);
				}
			}

		elseif ($hdr->fieldType == \waLibs\waDB::TIME)
			{
			if (strlen($cell->value))
				{
				echo substr($cell->value, 0, 2) . ":" . substr($cell->value, 3, 2);
				}
			}

		elseif ($hdr->fieldType == \waLibs\waDB::DECIMAL)
			{
			if (strlen($cell->value))
				{
				echo number_format($cell->value, 2, ",", ".");
				}
			}

//			elseif ($hdr->fieldType == \waLibs\waDB::INTEGER)
//				{
//				if (strlen($cell->value))
//					echo number_format ($cell->value, 0, ",", ".");
//				}
			
		else
			{
			echo nl2br(htmlspecialchars($cell->value));
			}
			
			?>
		</td>
		<?php

		}
		
	//**************************************************************************
	protected function getAlignment($hdr)
		{
		$alignment = "left";
		$alignment = $hdr->fieldType == \waLibs\waDB::DATE ? "center" : $alignment;
		$alignment = $hdr->fieldType == \waLibs\waDB::DATETIME ? "center" : $alignment;
		$alignment = $hdr->fieldType == \waLibs\waDB::TIME ? "center" : $alignment;
		$alignment = $hdr->fieldType == \waLibs\waDB::INTEGER ? "right" : $alignment;
		$alignment = $hdr->fieldType == \waLibs\waDB::DECIMAL ? "right" : $alignment;
		$alignment = $hdr->alignment == \waLibs\waTable::ALIGN_R ? "right" : $alignment;
		$alignment = $hdr->alignment == \waLibs\waTable::ALIGN_C ? "center" : $alignment;

		return $alignment;
		}
		
	//**************************************************************************
	protected function setHeaders()
		{
		
		?>
			
		<thead>
			<tr id='<?=$this->data->name?>_headers'>

				<?php
				$this->setHeadersData();
				$this->setRowActionsHeaders();
				?>

			</tr>
		</thead>
			
		<?php
		}
		
	//**************************************************************************
	protected function setHeadersData()
		{
		
		$qoe = strpos($this->data->uri, "?") === false ? "?" : "&";
		$tblName = $this->data->name;
			
		foreach ($this->data->columnHeaders as $idx => $hdr)
			{
			if (!$hdr->show)
				{
				continue;
				}
			?>
			<th 
				style='text-align: <?=$this->getAlignment($hdr)?>' 
				id='<?=$this->data->name?>_<?=$hdr->name?>'
				class='<?=$idx == 0 ? "id_cell" : "" ?>'
			>
				<?php
				if ($hdr->sort)
					{
					$sortMode = $hdr->quickSort == "asc" ? "desc" : "asc";
					echo "<a href='" . $this->data->uri . "$qoe" . "watable_qo[$tblName]=$hdr->name&watable_qom[$tblName]=$sortMode'>\n";
					echo $hdr->label;
					if ($hdr->quickSort != "no")
						{
						?>
						<img src='<?=$this->data->watablePath?>/uis/watable_default/img/<?=$hdr->quickSort?>_order.gif' border='0'/>
						<?php
						}
					echo "</a>\n";
					}
				else
					{
					echo $hdr->label;
					}
				?>
			</th>
			<?php
			}
		}
		
	//**************************************************************************
	protected function setRowActionsHeaders()
		{
		?>
		<th></th>
		<?php
		}
		
	//**************************************************************************
	protected function setJavascriptObjects()
		{
		$tblName = $this->data->name;

		?>
		
		<script type='text/javascript'>
			// inizializzazione parametri tabella <?=$tblName?>
			
			document.<?=$tblName?> = new waTable('<?=$tblName?>', '<?=$this->data->columnHeaders[0]->name?>', '<?=$this->data->exclusiveSelection ? 1 : 0 ?>', '<?=$this->data->formPage?>');

			<?php
			foreach ($this->data->rows as $row)
				{
				?>
				new waRow(document.<?=$tblName?>, '<?=$row->id?>', <?=$this->row2json($row)?>);
				<?php
				}
				
			// inizializzazione delle proprieta' delle colonne
			foreach ($this->data->columnHeaders as $idx => $hdr)
				{
				if (!$hdr->show) continue;
				?>
				new waColumn(document.<?=$tblName?>, "<?=$hdr->name?>", "<?=$hdr->label?>", "<?=$hdr->fieldType?>");
				<?php
				}
			?>

		</script>		
		
		<?php
		}
		
	//**************************************************************************
	protected function row2json($row)
		{
		$retval = array();
		
		foreach($row->cells as $idx => $cell)
			{
			$retval[$this->data->columnHeaders[$idx]->name] = $cell->value;
			}

		return json_encode($retval);
		}
		
	//**************************************************************************
	protected function getColType($col_name)
		{
		if ($idx = $this->getIdxFromName($this->data->columnHeaders, $col_name))
			{
			return $this->data->columnHeaders[$idx]->fieldType;
			}
			
		}
		
	//**************************************************************************
	// dato l'attributo name di un oggetto contenuto in un array, restituisce
	// l'indice dell'emento dell'array in cui il name è contenuto
	protected function getIdxFromName($array, $name, $name_property = "name")
		{
		foreach ($array as $idx => $item)
			{
			if ($item->$name_property == $name)
				{
				return $idx;
				}
			}
			
		return false;
		}
		
	//**************************************************************************
	}
//******************************************************************************


