<?php 
namespace webappointmentmanager;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waOptionView extends waControlView 
	{
	
	//**************************************************************************
	public function transform($data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		if (!$this->controlHaveLabel())
			{
			?>
			<div 
				class='waform_control_without_label'
				id='<?=$this->form->name?>_<?=$this->name?>_control_container' 
				style='text-align: right; <?=$this->getControlStyle()?>'
			>
			<?php
			}
			
		$idx = 0;
		foreach ($this->list as $key => $val)
			{
			list($realKey, $rest) = explode("|", $key);
			$checked = $realKey == $this->value ? "checked" : "";
			?>
			<input 
				type='radio' 
				id='<?=$this->form->name?>_<?=$this->name?>[<?=$idx?>]' 
				name='<?=$this->name?>' 
				value='<?=$key?>' 
				<?=$checked?> 
				<?=$this->getControlAttributes()?>
				style='<?=$this->getControlStyle()?>'
				class='form-control <?=$this->getControlClass()?>'
				>

			<label 
				id='waform_lblradio_<?=$this->form->name?>_<?=$this->name?>[<?=$idx?>]' 
				<?=$this->getControlAttributes()?>
				style='<?=$this->getControlStyle()?>'
			>
				<?=$val?>
			</label>
			
			<?php
			$idx++;
			}
			
		?>
		</div>
		<?php
		
		}
		
	//**************************************************************************
	}
//******************************************************************************


