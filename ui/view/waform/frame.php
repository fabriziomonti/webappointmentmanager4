<?php 
namespace webappointmentmanager;
	
require_once __DIR__ . "/control.php";

//******************************************************************************
class waFrameView extends waControlView 
	{
	
	//**************************************************************************
	public function transform($data)
		{
		parent::transform($data);
		$this->setControlHeader();
		
		$css = $this->readOnly ? "waform_disabled" : "";
		$css .= $this->mandatory ? " waform_mandatory" : "";
		$disabled = $this->readOnly ? "disabled" : "";
		
		?>
		<div 
			id='<?=$this->form->name?>_<?=$this->name?>' 
			<?=$this->getControlAttributes()?>
			style='<?=$this->getControlStyle()?>'
			class='<?=$this->getControlClass()?>'
		>

			<?php
			if ($this->value)
				{
				?>
				<span>
					<?=$this->value?>
				</span>
				<?php
			
				}
			?>
		</div>
		<?php
		
		}
		
	//**************************************************************************
	public function transformInput($data)
		{
		return null;
		}
		
	//**************************************************************************
	}
//******************************************************************************


