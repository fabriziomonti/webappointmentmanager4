<?php
include "webappointmentmanager.inc.php";

//*****************************************************************************
class page extends webappointmentmanager
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
		
		
	//**************************************************************************
	function __construct()
		{
		parent::__construct();
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->updateRecord();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Configurazione prenotazioni", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		
		$this->form = $this->getForm();
		$this->form->recordset = $this->getMyRecordset();
		$readOnly = false;
		
		$this->form->addInteger("DurataMediaMinuti", "Minuti durata media appuntamenti", $readOnly, true);
		$this->form->addInteger("MinutiPausa", "Minuti pausa tra appuntamenti", $readOnly, true)->value = 0;
		$this->form->addInteger("NrGiorniPrenotazione", "Nr. giorni minimo prenotazione", $readOnly, true)->value = 0;
		$this->form->addBoolean("ObbligoTelefono", "Obbligo indicazione recapito telefonico", $readOnly);
		$this->form->addBoolean("ObbligoIndirizzo", "Obbligo indicazione indirizzo reale", $readOnly);
		$this->form->addBoolean("ConfermaAutomatica", "Appuntamenti confermati automaticamente", $readOnly);

		$this->form_submitButtons($this->form, false, false);
		
		$this->form->getInputValues();

		}

	//***************************************************************************
	/**
	* -
	*
	* @return waLibs\waRecordset
	*/
	function getMyRecordset()
		{
		$dbconn = $this->getDBConnection();
		$sql = "select Config.*" .
				" from Config" .
				" where Config.IDUtente=" . $dbconn->sqlInteger($this->user->IDUtente);
			
		$recordset = $this->getRecordset($sql, $dbconn, 1);
		if (!$recordset->records)
			{
			$record = $recordset->add();
			$record->IDUtente = $this->user->IDUtente;
			}

		return $recordset;
		}
		
	//***************************************************************************
	function updateRecord()
		{
		$this->checkMandatory($this->form);
		
		$record = $this->form->recordset->records[0];
			
		$this->form->save();
		$this->setEditorData($record);
		$this->saveRecordset($record->recordset);

		$this->response();
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
